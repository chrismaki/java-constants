import java.io.*;
import chris_package.Constants;

public class Software_sales
{
	public static void main(String[] args) throws Exception{

		int numberOfCopies;
		double discount_rate = 0;
		char choice;
		double maintenance_fee = 0;
		BufferedReader keyboard;


		keyboard = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("How many copies would you like to buy: ");
		numberOfCopies = Integer.parseInt(keyboard.readLine());
		
		while(numberOfCopies < 0)
		{
			System.out.println("Error. The number of copies has to be at least 0.");
			System.out.print("How many copies would you like to buy: ");
			numberOfCopies = Integer.parseInt(keyboard.readLine());
		}

		if (numberOfCopies < Constants.DISCOUNT_LOWER_LIMIT_1) {
			discount_rate = 0;
		} else if (numberOfCopies < Constants.DISCOUNT_LOWER_LIMIT_2) {
			discount_rate = 0.2;
		} else if (numberOfCopies < Constants.DISCOUNT_LOWER_LIMIT_3) {
			discount_rate = 0.3;
		} else if (numberOfCopies < Constants.DISCOUNT_LOWER_LIMIT_4) {
			discount_rate = 0.4;
		} else {
			discount_rate = 0.5;
		}

		System.out.printf("The discount is $%.2f\n", numberOfCopies * Constants.PACKAGE_PRICE * discount_rate);
		System.out.printf("The total amount of the purchase is $%.2f\n", numberOfCopies * Constants.PACKAGE_PRICE * (1 - discount_rate));
		
		System.out.print("What after-sale maintenance service plan would like you to buy(A, B, C): ");
		choice = keyboard.readLine().charAt(0);
		switch(choice)
		{
			case 'A':
				maintenance_fee = Constants.MAINTENANCE_PRICE_A;
				break;
			case 'B':
				maintenance_fee = Constants.MAINTENANCE_PRICE_B;
				break;
			case 'C':
				maintenance_fee = Constants.MAINTENANCE_PRICE_C;
				break;
			default:
				System.out.println("Error. Wrong choice. A or B or C");
				System.exit(13);
		}
		System.out.printf("The service charge is $%.2f\n", maintenance_fee);
	}
}