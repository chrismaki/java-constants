// name: Krisztian Markella
// using JOptionPane

import javax.swing.JOptionPane;
import chris_package.Constants;

public class SoftwareSalesJOptionPane
{
	public static void main(String[] args) throws Exception{

		int numberOfCopies;
		double discount_rate = 0;
		char choice;
		double maintenance_fee = 0;
		String str;

		str = JOptionPane.showInputDialog("How many copies would you like to buy: ");
		numberOfCopies = Integer.parseInt(str);
		
		while(numberOfCopies < 0)
		{
			str = JOptionPane.showInputDialog("Error. The number of copies has to be at least 0." + 
											  "\nHow many copies would you like to buy: ");
			numberOfCopies = Integer.parseInt(str);
		}

		if (numberOfCopies < Constants.DISCOUNT_LOWER_LIMIT_1) {
			discount_rate = 0;
		} else if (numberOfCopies < Constants.DISCOUNT_LOWER_LIMIT_2) {
			discount_rate = 0.2;
		} else if (numberOfCopies < Constants.DISCOUNT_LOWER_LIMIT_3) {
			discount_rate = 0.3;
		} else if (numberOfCopies < Constants.DISCOUNT_LOWER_LIMIT_4) {
			discount_rate = 0.4;
		} else {
			discount_rate = 0.5;
		}

		JOptionPane.showMessageDialog(null, 
			"The discount is $" + 
				numberOfCopies * Constants.PACKAGE_PRICE * discount_rate + "\n" +
			"The total amount of the purchase is $" + 
				numberOfCopies * Constants.PACKAGE_PRICE * (1 - discount_rate) + "\n");
		
		str = JOptionPane.showInputDialog("What after-sale maintenance service plan would like you to buy(A, B, C): ");

		choice = str.charAt(0);
		switch(choice)
		{
			case 'A':
				maintenance_fee = Constants.MAINTENANCE_PRICE_A;
				break;
			case 'B':
				maintenance_fee = Constants.MAINTENANCE_PRICE_B;
				break;
			case 'C':
				maintenance_fee = Constants.MAINTENANCE_PRICE_C;
				break;
			default:
				JOptionPane.showMessageDialog(null, "Error. Wrong choice. A or B or C");
				System.exit(13);
		}
		JOptionPane.showMessageDialog(null, "The service charge is $" + maintenance_fee + "\n");
		System.exit(0);
	}
}