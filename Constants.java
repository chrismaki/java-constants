package chris_package;

public final class Constants
{
	public static final double PACKAGE_PRICE = 99;
	public static final double MAINTENANCE_PRICE_A = 24.99;
	public static final double MAINTENANCE_PRICE_B = 59.99;
	public static final double MAINTENANCE_PRICE_C = 199.99;

	public static final int DISCOUNT_LOWER_LIMIT_1 = 10;
	public static final int DISCOUNT_LOWER_LIMIT_2 = 20;
	public static final int DISCOUNT_LOWER_LIMIT_3 = 50;
	public static final int DISCOUNT_LOWER_LIMIT_4 = 100;
	private Constants()
	{}
}
